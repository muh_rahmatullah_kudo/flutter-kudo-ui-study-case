
# Kudo replica

## Run the app

1. Download and install flutter and dart plugin to your fav ide
2. (optional) Open the project
3. flutter emulators --launch <DEVICE-ID>
4. flutter run

## Screenshot

![picture](screenshots/kudo_ss.png)