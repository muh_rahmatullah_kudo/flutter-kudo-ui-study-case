import 'package:flutter/cupertino.dart';

class ButtonKit extends StatelessWidget {
  final IconData icon;
  final String title;

  const ButtonKit({
    Key key,
    @required this.icon,
    @required this.title,
  })  : assert(icon != null),
        assert(title != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(14.0),
      child: Center(
        child: Container(
          child: Column(
            children: <Widget>[
              Icon(
                icon,
                size: 44.0,
                color: Color(0xFFFFFFFF),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 6.0),
                child: Text(
                  title,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Color(0xFFFFFFFF),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
